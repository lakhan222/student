# Student Project

This Project will store students login and logout time.
This project is build using Python, Django and Sqlite database.

## Features

- It contains four API's. 1.Register, 2.Login, 3.Logout, 4.Data of Student

## Tech

This project uses multiple open source to make setup:

- [Python] - As Backend Language
- [Django] - As Backend Framework
- [Django Restframework] - For creating API's and integrating with Django
- [Sqlite Database] - As light-weight and default supported databases

## Installation

This project requires [NPython3.6](https://www.python.org/downloads/release/python-360/)

Install the dependencies and devDependencies and start the server.

```sh
cd student
pip3 install requirements.txt
python manage.py runserver
```

## Plugins

Dillinger is currently extended with the following plugins.
Instructions on how to use them in your own application are linked below.

| API | Method | Description | URL | Payload |
| ------ | ------ | ------ | ------ | ------ |
| Register | POST | For student registration | [http://127.0.0.1:8000/api/student/register/][PlDb] | {"name": "test1", "user_name": "test1", "password":"password123"} |
| Login |  POST | For student Login | [http://127.0.0.1:8000/api/student/login/][PlDb] | {"user_name": "test1", "password":"password123"} |
| Logout | GET | For student Logout(Bearer token in Authorization) | [http://127.0.0.1:8000/api/student/logout/][PlDb] |  |
| Student Data | GET | For student Data(Bearer token in Authorization) | [http://127.0.0.1:8000/api/student/data/][PlDb] |  |


Verify the deployment by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:8000
```



